package top.cavecraft.purgatoryotf;

import net.md_5.bungee.api.config.ServerInfo;

import java.net.InetSocketAddress;
import java.util.ArrayList;

import static top.cavecraft.purgatoryotf.util.ServerUtils.addServer;

public class OTFServer {
    static ArrayList<OTFServer> allOTFServers = new ArrayList<>();

    private boolean shutdown = false;
    ServerInfo serverInfo;
    String port;

    OTFServer(InetSocketAddress address) {
        serverInfo = addServer(address);
        allOTFServers.add(this);
    }

    public void stop() {
        /*
         Ew, I don't really have a way of sending a shutdown message
         to the server without also adding the ability for a hacker
         to shut down the server remotely or doing some crazy key auth stuff.
         */
        shutdown = true;
    }

    public boolean checkIfShutdown() {
        return shutdown;
    }
}
